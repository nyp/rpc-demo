/*
 * @(#) RPCEncoder
 * 版权声明 厦门畅享信息技术有限公司, 版权所有 违者必究
 *
 * <br> Copyright:  Copyright (c) 2018
 * <br> Company:厦门畅享信息技术有限公司
 * <br> @author ningyp
 * <br> 2018-11-11 21:21:53
 * <br> @version 1.0
 * ————————————————————————————————
 *    修改记录
 *    修改者：
 *    修改时间：
 *    修改原因：
 * ————————————————————————————————
 */

package com.yope.rpc.core.netty;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * @author yope
 * @date 2018/11/11
 */
public class RPCEncoder extends MessageToByteEncoder {

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, Object o, ByteBuf byteBuf) throws Exception {

        // 将java对象序列化成为byte数组
        byte[] bytes = SerializationUtil.serilize(o);

        // 写入byte数组长度, 方便反序列化时判断是否接收完整
        byteBuf.writeInt(bytes.length);
        byteBuf.writeBytes(bytes);
    }
}
