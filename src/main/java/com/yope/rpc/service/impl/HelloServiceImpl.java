/*
 * @(#) HelloServiceImpl
 * 版权声明 厦门畅享信息技术有限公司, 版权所有 违者必究
 *
 * <br> Copyright:  Copyright (c) 2018
 * <br> Company:厦门畅享信息技术有限公司
 * <br> @author ningyp
 * <br> 2018-11-11 20:45:41
 * <br> @version 1.0
 * ————————————————————————————————
 *    修改记录
 *    修改者：
 *    修改时间：
 *    修改原因：
 * ————————————————————————————————
 */

package com.yope.rpc.service.impl;

import com.yope.rpc.service.api.HelloService;

/**
 * @author yope
 * @date 2018/11/11
 */
public class HelloServiceImpl implements HelloService {
    @Override
    public String hello(String str) {
        System.out.println(this.getClass().getName() + ":" + str);
        return "hello "+str;
    }
}
